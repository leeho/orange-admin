import SystemController from './Controller/SystemController'
import SysDataPermController from './Controller/SysDataPermController'
import DictionaryController from './Controller/DictionaryController'
import SysDeptController from './Controller/SysDeptController.js';
import SysUserController from './Controller/SysUserController.js';

export {
  SystemController,
  SysDataPermController,
  DictionaryController,
  SysDeptController,
  SysUserController
}
